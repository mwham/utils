# Fake qsub

This is a script that's meant to sit between NextFlow and UGE on Eddie. Eddie allocates memory using `h_vmem`,
which the SgeExecutor in NextFlow doesn't specify. You can configure it to use CrgExecutor, but then h_vmem gets
multiplied by the number of cores requested to a total - which shouldn't happen because on Eddie,`h_vmem`
should be per core.

## Usage

To use this, the link `./qsub` should be the first qsub in `$PATH`. To get this, prepend the directory
containing it:

    export PATH=path/to/utils/fake_qsub:$PATH

The fake qsub reads the UGE script, finds `h_vmem`, adjusts it for the number of cpus requested, and then runs
the real qsub with the correct `h_vmem` as a command arg, which overrides the wrong one in the script.

It would be possible to add the fake qsub as a setuptools script/entry point, but since this would put the resulting
entry point in `python/bin/`, this would override other executables like `python` and `pip`. Therefore, this should be
installed from source instead.
