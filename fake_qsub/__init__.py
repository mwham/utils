#!/bin/env python3
"""
Fake qsub script to be inserted between NextFlow and UGE on Eddie. Finds the specified h_vmem, corrects it for the
number of cpus requested, and submits the script with the correct h_vmem as a command line arg, which should override
the value in the script front matter.

To use, *prepend* the directory containing this script to $PATH.

Author: Murray Wham (MRC HGU, IGMM), Apr 2020
"""

import sys
import subprocess
import argparse

# SGE multipliers available on Eddie - see https://www.wiki.ed.ac.uk/display/ResearchServices/Memory+Specification
multipliers = {
    'k': 1000,
    'K': 1024,
    'm': 1000**2,
    'M': 1024**2,
    'g': 1000**3,
    'G': 1024**3,
    '': 1
}


def parse_mem(mem):
    """
    Convert a multiplied memory value to a number of bytes
    :param str mem: e.g, '2G', '1024M', '4g'
    """
    multiplier = mem[-1]
    if multiplier in multipliers:
        mem = mem[:-1]  # trim off the multiplier
    else:
        multiplier = ''

    multiplication_factor = multipliers[multiplier]
    return int(float(mem) * multiplication_factor), multiplier


def factorise(mem_in_bytes, multiplier, dec_places=2):
    """
    Convert a number of bytes to a value in, e.g, kilobytes, gigabytes, etc.
    :param float/int mem_in_bytes:
    :param str multiplier: See `multipliers` for available options.
    :param int dec_places: SGE accepts mem values as floats - round the result differently if desired.
    """
    multiplication_factor = multipliers[multiplier]
    factorised = mem_in_bytes / multiplication_factor
    if factorised == int(factorised):
        factorised = int(factorised)
    else:
        factorised = round(factorised, dec_places)

    return '%s%s' % (factorised, multiplier)


def extract_script_opts(script):
    script_opts = {}
    with open(script) as f:
        for line in f:
            if line.startswith('#!'):  # shebang
                pass
            elif line.startswith('#$ '):  # script option
                opts = line[3:].rstrip('\n').split(' ')
                script_opts[opts[0]] = opts[1:]
            else:  # rest of script
                break

    return script_opts


def correct_h_vmem(script_opts):
    if '-l' not in script_opts:
        return None
    
    penv, cpus = script_opts.get('-pe', [None, 1])
    cpus = int(cpus)

    # e.g, 'h_vmem=256m,virtual_free=256m' -> {'h_vmem': '256m', 'virtual_free': '256m'} -> (256000000, 'm')
    mem_opts = dict(o.split('=') for o in script_opts['-l'][0].split(','))
    h_vmem_in_bytes, multiplier = parse_mem(mem_opts['h_vmem'])
    return factorise(h_vmem_in_bytes / cpus, multiplier)
    

def main(argv=None):
    a = argparse.ArgumentParser()
    a.add_argument('-terse', action='store_true')
    a.add_argument('script')
    args = a.parse_args(argv)

    script_opts = extract_script_opts(args.script)
    new_h_vmem = correct_h_vmem(script_opts)

    # this script should be the first 'qsub' in $PATH, so the second one is the real qsub
    qsubs = subprocess.check_output(['which', '-a', 'qsub']).decode().rstrip('\n').split('\n')

    # have to use sys.argv here, not argv
    assert qsubs[0] == sys.argv[0], 'This script needs to be the first qsub in $PATH'

    cmd = [qsubs[1]]
    if new_h_vmem:
        cmd.extend(['-l', 'h_vmem=%s' % new_h_vmem])
    if args.terse:
        cmd.append('-terse')
    cmd.append(args.script)

    # the real qsub prints the job number to stdout, so shouldn't need to worry about that here
    return subprocess.check_call(cmd)


if __name__ == '__main__':
    sys.exit(main())
