# mwham/Scripts

Collection of scripts and utilities for work at MRC HGU.

Contents:

- fake_qsub: Sits in front of qsub on the PATH and translates memory specifications properly for using NextFlow on Eddie
- ped_parsing: Parsing and validation of family data from .ped files

This module currently only relies on the Python 3 standard library.

## Python


These utilities were written for Python 3.7. Other versions of Python 3 will probably work as well.


## Tests

The utilities here are unit tested. The tests are run automatically on GitLab via `.gitlab-ci.yml`, but can also be run
locally:

    $ python -m unittest
    ........
    ----------------------------------------------------------------------
    Ran 8 tests in 0.005s
    
    OK
