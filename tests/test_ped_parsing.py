import unittest
from unittest.mock import patch
from utils.ped_parsing import PedFile, Family, Individual

ped_info = [
    # family            id      father      mother      sex     phenotype
    'Trio               a       0           0           2       1',
    'Trio               b       0           0           1       1',
    'Trio               c       b           a           1       2',
    'Quad               d       0           0           2       1',
    'Quad               e       0           0           1       1',
    'Quad               f       e           d           1       2',
    'Quad               g       e           d           2       2',
    'Singleton          h       0           0           2       2',
    'ParentChildDuo     i       0           0           2       1',
    'ParentChildDuo     j       0           i           2       2',
    'SingleParent       m       0           0           1       1',
    'SingleParent       n       m           0           1       1',
    'SingleParent       o       m           0           2       1',
    'MultiLevel         p       0           0           2       1',  # grandparent
    'MultiLevel         q       0           p           1       1',
    'MultiLevel         r       0           0           2       1',
    'MultiLevel         s       q           r           2       2',
    'TrioAffectedParent t       0           0           2       2',
    'TrioAffectedParent u       0           0           1       1',
    'TrioAffectedParent v       u           t           2       2',
    'SharedAffected     w       0           0           2       2',
    'SharedAffected     x       0           w           2       2'
]


class TestPedFile(unittest.TestCase):
    def setUp(self):
        with patch('builtins.open') as patched_open:
            patched_open.return_value.__enter__.return_value = ped_info
            self.ped_file = PedFile('a_ped_file')

    def test_resolve_relationships(self):
        f = self.ped_file.families['Trio'].members
        self.assertIs(f['c'].mother, f['a'])
        self.assertIs(f['c'].father, f['b'])
        self.assertIsNone(f['a'].mother)
        self.assertIsNone(f['a'].father)
        self.assertIsNone(f['b'].mother)
        self.assertIsNone(f['b'].father)
        self.assertListEqual(f['c'].children, [])

        f = self.ped_file.families['Quad'].members
        self.assertIs(f['f'].mother, f['d'])
        self.assertIs(f['f'].father, f['e'])
        self.assertIs(f['g'].mother, f['d'])
        self.assertIs(f['g'].father, f['e'])
        self.assertListEqual(f['f'].siblings, [f['g']])
        self.assertListEqual(f['g'].siblings, [f['f']])

        f = self.ped_file.families['Singleton'].members
        self.assertIsNone(f['h'].mother)
        self.assertIsNone(f['h'].father)
        self.assertListEqual(f['h'].children, [])

        f = self.ped_file.families['ParentChildDuo'].members
        self.assertIs(f['j'].mother, f['i'])

        f = self.ped_file.families['SingleParent'].members
        self.assertIs(f['n'].father, f['m'])
        self.assertListEqual(f['n'].siblings, [f['o']])
        self.assertListEqual(f['o'].siblings, [f['n']])

        f = self.ped_file.families['MultiLevel'].members
        self.assertIs(f['q'].mother, f['p'])
        self.assertIsNone(f['r'].father)
        self.assertIs(f['s'].mother, f['r'])
        self.assertIs(f['s'].father, f['q'])

        f = self.ped_file.families['TrioAffectedParent'].members
        self.assertIs(f['v'].mother, f['t'])
        self.assertIs(f['v'].father, f['u'])
        self.assertIs(f['t'].children[0], f['v'])

        f = self.ped_file.families['SharedAffected'].members
        self.assertIs(f['x'].mother, f['w'])
        self.assertIs(f['w'].children[0], f['x'])
        self.assertTrue(all(m.phenotype == 2 for m in f.values()))

    def test_family_types(self):
        # each family should have its corresponding Family test method(s) return True, everything else should be False
        tests = (
            'is_singleton', 'is_duo', 'is_trio', 'is_quad', 'is_simple_family', 'is_shared_affected',
            'is_affected_parent'
        )
        families_and_tests = {
            'Singleton': ['is_singleton'],
            'ParentChildDuo': ['is_duo'],
            'Trio': ['is_trio', 'is_simple_family'],
            'Quad': ['is_quad', 'is_simple_family'],
            'SingleParent': ['is_single_parent'],
            'MultiLevel': [],
            'TrioAffectedParent': ['is_trio', 'is_simple_family', 'is_affected_parent'],
            'SharedAffected': ['is_duo', 'is_shared_affected']
        }

        for family, true_tests in families_and_tests.items():
            for t in tests:
                if t in true_tests:
                    assertion = self.assertTrue
                else:
                    assertion = self.assertFalse

                method_to_test = getattr(self.ped_file.families[family], t)
                assertion(method_to_test(), msg='%s.%s -> %s' % (family, t, assertion.__name__))


class TestIndividual(unittest.TestCase):
    """Doesn't test self.resolve_relationships because this is done more effectively above."""

    def setUp(self):
        self.father = Individual('f1', 'father', '0', '0', '1', '1')
        self.mother = Individual('f1', 'mother', '0', '0', '2', '1')
        self.individual = Individual('f1', 'individual', 'father', 'mother', '1', '1')
        self.sibling = Individual('f1', 'sibling', 'father', 'mother', '2', '1')
        self.half_sibling = Individual('f1', 'half_sibling', '0', 'mother', '2', '1')
        self.f1 = Family(
            'f1',
            father=self.father,
            mother=self.mother,
            individual=self.individual,
            sibling=self.sibling,
            half_sibling=self.half_sibling
        )

        self.other_mother = Individual('f2', 'other_mother', '0', '0', '2', '1')
        self.other = Individual('f2', 'other', '0', 'other_mother', '2', '1')
        self.f2 = Family(
            'f2',
            other_mother=self.other_mother,
            other=self.other
        )

    def test_is_sibling_of(self):
        self.assertTrue(self.individual._is_sibling_of(self.sibling))
        self.assertTrue(self.individual._is_sibling_of(self.half_sibling))
        self.assertFalse(self.individual._is_sibling_of(self.other))
        self.assertFalse(self.mother._is_sibling_of(self.other_mother))

    def test_eq(self):
        self.assertEqual(self.individual.mother, self.mother)
        self.assertNotEqual(self.individual.mother, self.father)
        self.assertNotEqual(self.individual, None)
