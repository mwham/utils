import unittest
import fake_qsub
from unittest.mock import patch

obs_and_exp = (
    ('2G', (2147483648, 'G')),
    ('2g', (2000000000, 'g')),
    ('128M', (134217728, 'M')),
    ('128m', (128000000, 'm')),
    ('64K', (65536, 'K')),
    ('64k', (64000, 'k')),
    ('0.5M', (524288, 'M')),
    ('0.5m', (500000, 'm')),
    ('1024', (1024, ''))
)


class TestFakeQSub(unittest.TestCase):
    def test_parse_mem(self):
        for arg, exp in obs_and_exp:
            self.assertTupleEqual(fake_qsub.parse_mem(arg), exp)

    def test_factorise(self):
        for exp, args in obs_and_exp:
            self.assertEqual(fake_qsub.factorise(*args), exp)

    @patch('builtins.open')
    def test_extract_script_opts(self, patched_open):
        # nothing specified
        patched_f = patched_open.return_value.__enter__.return_value = [
            '#!/bin/bash',
            '#$ -l h_vmem=2048M,virtual_free=2048M',
            '#$ -pe sharedmem 3',
            'rest of script'
        ]
        self.assertDictEqual(
            fake_qsub.extract_script_opts('script.sh'),
            {'-l': ['h_vmem=2048M,virtual_free=2048M'], '-pe': ['sharedmem', '3']}
        )

    def test_correct_h_vmem(self):
        arg = {}
        self.assertEqual(fake_qsub.correct_h_vmem(arg), None)

        arg['-l'] = ['h_vmem=2048M,virtual_free=2048M']
        self.assertEqual(fake_qsub.correct_h_vmem(arg), '2048M')

        arg['-pe'] = ['sharedmem', 4]
        self.assertEqual(fake_qsub.correct_h_vmem(arg), '512M')

    @patch('sys.argv', new=['/fake/qsub', '-terse', 'script.sh'])
    @patch('fake_qsub.extract_script_opts')
    @patch('fake_qsub.correct_h_vmem', return_value=None)
    @patch('subprocess.check_output')
    @patch('subprocess.check_call', return_value=0)
    def test_main(self, p_check_call, p_check_output, p_correct, p_extract):
        args = ['-terse', 'script.sh']

        p_check_output.return_value = b'/real/qsub\n/fake/qsub\n'
        with self.assertRaises(AssertionError):
            fake_qsub.main(args)

        p_check_output.return_value = b'/fake/qsub\n/real/qsub\n'
        self.assertEqual(fake_qsub.main(args), 0)
        p_check_call.assert_called_with(['/real/qsub', '-terse', 'script.sh'])
        p_check_call.reset_mock()

        # stil get same results with sys.argv
        self.assertEqual(fake_qsub.main(), 0)
        p_check_call.assert_called_with(['/real/qsub', '-terse', 'script.sh'])
        p_check_call.reset_mock()

        p_correct.return_value = '1024M'
        self.assertEqual(fake_qsub.main(args), 0)
        p_check_call.assert_called_with(['/real/qsub', '-l', 'h_vmem=1024M', '-terse', 'script.sh'])
