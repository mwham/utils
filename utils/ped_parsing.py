#!/usr/bin/env python3
# Ped file parsing from mwham/Utils

import os
import argparse


class PedFile:
    """
    Loads Individual objects from a Ped file and stores Family objects.
    Usage:
    >>> ped_file = PedFile('samples.ped')
    >>> ped_file.families['a_family'].members['a_member']
    Individual('a_member')
    """
    def __init__(self, ped_file):
        self.ped_file = ped_file
        self.ped_lines = []
        self.families = {}

        with open(ped_file) as f:
            for line in f:
                family_id, individual, father, mother, sex, phenotype = line.split()
                if family_id not in self.families:
                    self.families[family_id] = Family(family_id)

                self.families[family_id].add_member(
                    Individual(family_id, individual, father, mother, sex, phenotype)
                )

        for family in self.families.values():
            family.resolve_relationships()

    def __repr__(self):
        return "PedFile('%s')" % self.ped_file


class Family:
    """
    Stores Individuals and determines relationships between them. Two alternate usages:
    1. Create with a name and `name: Individual` pair via **kwargs
    2. Create with a name only, then add Individuals with add_member(), then call resolve_relationships()
    """
    def __init__(self, name, **members):
        self.name = name
        self.members = members
        if self.members:
            for m in self.members.values():
                if m.family_id != self.name:
                    print('Warning: resetting family ID for %s to %s' % (m.name, self.name))
                    m.family_id = self.name

            self.resolve_relationships()

    def add_member(self, individual):
        assert individual.name not in self.members
        self.members[individual.name] = individual

    def resolve_relationships(self):
        """
        In children, transform parents from strings (e.g, mother='a_parent') to objects (mother=Individual('a_parent')).
        In parents, populate self.children with Individuals.
        """
        for individual in self.members.values():
            individual.resolve_relationships(self)

    def is_duo(self):
        """True if the family is one parent and one child."""
        return self.is_single_parent() and len(self.members) == 2

    def is_single_parent(self):
        """True if one parent and 1+ children."""

        members = self.members.values()
        parents = [m for m in members if m.children and not m.mother and not m.father]
        children = [m for m in members if not m.children and (m.mother or m.father)]

        return len(parents) == 1 and children

    def is_simple_family(self, check_affected_parent=False):
        """True if two parents and 1+ children. Test for affected parents if specified"""

        if len(self.members) < 3:  # duo/singleton
            return False

        mother = None
        father = None
        children = []

        for m in self.members.values():
            if m.mother and m.father and not m.children:
                children.append(m)
            elif m.children:
                if m.mother or m.father:
                    # already a mother/father defined - not a simple family
                    return False

                if m.sex == 2:
                    mother = m
                elif m.sex == 1:
                    father = m
                else:
                    return False

        if not all((mother, father, children)):
            return False

        if check_affected_parent and mother.phenotype != 2 and father.phenotype != 2:
            return False

        return True

    def is_affected_parent(self):
        return self.is_simple_family(check_affected_parent=True)

    def is_trio(self):
        """Classic parents-child trio."""
        return self.is_simple_family() and len(self.members) == 3

    def is_quad(self):
        """2 parents, 2 children"""
        return self.is_simple_family() and len(self.members) == 4

    def is_singleton(self):
        """And singletons."""
        return len(self.members) == 1

    def is_shared_affected(self):
        """Potentially semi-related individuals or parent-child duo, all affected"""

        return len(self.members) > 1 and all(i.phenotype == 2 for i in self.members.values())

    def to_ped(self):
        return '\n'.join(
            self.members[m].to_ped()
            for m in sorted(self.members)
        ) + '\n'

    def __repr__(self):
        return "Family('%s', members=%i)" % (self.name, len(self.members))

    def __eq__(self, other):
        return self.name == other.name and sorted(self.members.values()) == sorted(other.members.values())


class Individual:
    def __init__(self, family_id: str, name: str, father_id, mother_id, sex, phenotype):
        self.family_id = family_id
        self.name = name
        self.father = None if str(father_id) == '0' else father_id
        self.mother = None if str(mother_id) == '0' else mother_id
        self.sex = int(sex)
        self.phenotype = int(phenotype)

        self.children = []
        self.siblings = []

    def resolve_relationships(self, family):
        if self.mother:
            assert isinstance(self.mother, str)  # haven't already done the resolve
            mother = family.members[self.mother]
            assert mother.sex == 2, 'Unexpected sex found for %s (mother): %s' % (self.mother, mother.sex)
            mother.children.append(self)
            self.mother = mother  # replace the string ID with the actual object

        if self.father:
            assert isinstance(self.father, str)
            father = family.members[self.father]
            assert father.sex == 1, 'Unexpected sex found for %s (father): %s' % (self.father, father.sex)
            father.children.append(self)
            self.father = father

        for name, other in family.members.items():
            if name == self.name:
                continue

            if self._is_sibling_of(other):
                self.siblings.append(other)

    def _is_sibling_of(self, other):
        """
        Compare an individual with another. This technically picks up half-siblings as well.
        """
        # can't discern siblings if no parents listed
        if not self.mother and not self.father:
            return False

        if not other.mother and not other.father:  # no parents listed, can't discern
            return False

        def get_parent(parent):
            if isinstance(parent, Individual):
                return parent.name
            return parent

        return (get_parent(self.mother) == get_parent(other.mother) or
                get_parent(self.father) == get_parent(other.father))

    def copy(self, reset_parents=False):
        return Individual(
            self.family_id,
            self.name,
            self.father.name if self.father and not reset_parents else None,
            self.mother.name if self.mother and not reset_parents else None,
            self.sex,
            self.phenotype
        )

    def to_ped(self):
        return '\t'.join(
            (
               self.family_id,
               self.name,
               self.father.name if self.father else '0',
               self.mother.name if self.mother else '0',
               str(self.sex),
               str(self.phenotype)
            )
        )

    def __repr__(self):
        return "Individual('%s')" % self.name

    def __eq__(self, other):
        if self.__class__ != other.__class__:
            return False

        attrs = ('family_id', 'name', 'father', 'mother', 'sex', 'phenotype', 'children', 'siblings')
        return all(getattr(self, a) == getattr(other, a) for a in attrs)

    def __lt__(self, other):
        return self.name < other.name
