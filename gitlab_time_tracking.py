import os
import sys
import json
import requests
import datetime
import argparse
from getpass import getuser
from collections import defaultdict

BASEURL = 'https://git.ecdf.ed.ac.uk/api/v4'
GRAPH_BASEURL = 'https://git.ecdf.ed.ac.uk/api/graphql'
QUERY = '''
query timeLogReport($user: String!, $start: Time!, $end: Time!, $pageSize: Int = 10, $cursor: String) {
  timelogs(
    username: $user
    startDate: $start
    endDate: $end
    first: $pageSize
    after: $cursor
  ) {
    count
    edges {
      node {
        id
        timeSpent
        spentAt
        summary
        issue {
          id
          title
          webUrl
        }
      }
    }
    pageInfo {
      endCursor
      hasNextPage
    }
  }
}
'''

session = requests.Session()
token_file = os.path.join(os.path.dirname(__file__), 'readonly.tok')

with open(token_file) as f:
    session.params['access_token'] = f.read().strip()


def _print(j):
    json.dump(j, sys.stdout, indent=2)


def get_timelogs(user, start, end, page_size=40, cursor=None):
    r = session.get(
        GRAPH_BASEURL,
        json={
            'query': QUERY,
            'variables': {
                'user': user,
                'start': start.isoformat(),
                'end': end.isoformat(),
                'pageSize': page_size,
                'cursor': cursor
            }
        }
    ).json()['data']['timelogs']
    for e in r['edges']:
        yield e['node']

    if r['pageInfo'].get('hasNextPage'):
        for node in get_timelogs(user, start, end, page_size, r['pageInfo']['endCursor']):
            yield node


def main():
    today = datetime.date.today()
    a = argparse.ArgumentParser()
    a.add_argument('--start')
    a.add_argument('--end')
    a.add_argument('--page_size', type=int, default=40)
    args = a.parse_args()
    
    if args.end:
        y, m, d = (int(x) for x in args.end.split('-'))
        t1 = datetime.datetime(y, m, d, 23, 59, 59)  # end of day
    else:
        t1 = datetime.datetime.now()

    if args.start:
        y, m, d = (int(x) for x in args.start.split('-'))
        t0 = datetime.datetime(y, m, d)
        td = t1 - t0
    else:
        td = datetime.timedelta(days=7)
        _t0 = t1 - td
        t0 = datetime.datetime(_t0.year, _t0.month, _t0.day, 0, 0, 0)  # start of day

    print('Showing time entries for %s days between %s and %s' % (td.days, t0, t1))

    timelogs = list(get_timelogs(getuser(), t0, t1, args.page_size))
    timelogs_by_issue = defaultdict(list)
    earliest_activity = None
    latest_activity = None

    for t in timelogs:
        dt = datetime.datetime.fromisoformat(t['spentAt'])
        spent_at = datetime.date(dt.year, dt.month, dt.day)
        timelogs_by_issue[t['issue']['title']].append(round(t['timeSpent'] / 3600, 3))  # seconds -> hours
        if earliest_activity is None or spent_at < earliest_activity:
            earliest_activity = spent_at

        if latest_activity is None or spent_at > latest_activity:
            latest_activity = spent_at

    for k in sorted(timelogs_by_issue):
        print('%s: %sh' % (k, round(sum(timelogs_by_issue[k]), 3)))

    print('Earliest activity: %s' % earliest_activity)
    print('Latest activity: %s' % latest_activity)
    print('Total: %sh' % round(sum(sum(v) for v in timelogs_by_issue.values()), 3))

if __name__ == '__main__':
    main()

