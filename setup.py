import setuptools
from utils import __version__

with open('Readme.md') as f:
    long_description = f.read()


setuptools.setup(
    name='Utils',
    version=__version__,
    description='Collection of scripts and utilities for work at MRC HGU',
    long_description=long_description,
    long_description_content_type='text/markdown',
    author='Murray Wham',
    author_email='murray.wham@ed.ac.uk',
    keywords='qsub nextflow pedfile opencga',
    packages=setuptools.find_packages(exclude=('tests', 'fake_qsub')),
    entry_points={
        'console_scripts': [
            'qsub = fake_qsub:main'
        ]
    }
)
